import Express from "express"
import Logger from "morgan"
import CookieParser from "cookie-parser"
import BodyParser from "body-parser"
import helmet from "helmet"
import cors from "cors"

const app = Express()

app.use(helmet())
app.use(cors({
  origin: "*",
  optionsSuccessStatus: 200,
  "credentials": true
}))

// Logger
app.use(Logger("dev"))

// BodyParser
app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))

// CookieParser
app.use(CookieParser())

// Express
app.set("port", process.env.PORT || 3000)
app.disable("x-powered-by")

//Routes
import Index from "./routes/Index"
app.use("/", Index)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handler
app.use(function(err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get("env") === "development" ? err : {}

  // render the error page
  res.status(err.status || 500)
})

// Start Server
app.listen(app.get("port"), function(){
  console.log("Listening on port:", app.get("port"))
})

export default app
