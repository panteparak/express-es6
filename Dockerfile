FROM node:6.11.1
ADD . /app
WORKDIR /app
ENV NODE_ENV=production
RUN npm install -g pm2 && \ 
	npm install && \
	npm run build
	
EXPOSE 3000
CMD ["pm2-docker", "dist/app.js"]
